#Download base image ubuntu 22.04
FROM docker.io/ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && \
    apt-get -qq install \
        man-db wget vim nano flex byacc git \
        binutils autoconf automake make cmake \
        qemu-system-x86 nasm gcc \
        g++-multilib gdb libnuma-dev numactl \
        mpich liburing-dev

ENV EDITOR=vim

# Switch back to dialog for any ad-hoc use of apt-get
ENV DEBIAN_FRONTEND=dialog
