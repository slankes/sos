#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#define COLUMNS   1024
#define ROWS      1024
#define MAX_ITERATIONS  1000

// communication tags
#define DOWN     100
#define UP       101 

#define TOLERANCE 0.01

int main(int argc, char** argv) {
    int i, j;
    int iterations=0;
    int error_code = 0;
    double** A_new = NULL;
    double** A = NULL;

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (ROWS % world_size != 0) {
        if (world_rank == 0) {
            fprintf(stderr, "ROWS must be dividable by %d\n", world_size);
        }
        error_code = 1;
        goto leave;
    }
    int rows_per_proc = ROWS / world_size;

    A_new = malloc(sizeof(double*) * (rows_per_proc+2));
    if (A_new == NULL) {
        error_code = 1;
        goto leave;
    }

    A = malloc(sizeof(double*) * (rows_per_proc+2));
    if (A == NULL) {
        error_code = 1;
        goto leave;
    }

    for (i = 0; i<rows_per_proc+2; i++) {
        A_new[i] = malloc(sizeof(double) * (COLUMNS+2));
        if (A_new[i] == NULL) {
            error_code = 1;
            goto leave;
        }

        A[i] = malloc(sizeof(double) * (COLUMNS+2));
        if (A[i] == NULL) {
            error_code = 1;
            goto leave;
        }
    }

    // Initialize A
    for (i = 0; i <= rows_per_proc+1; i++) {
        for (j = 0; j <= COLUMNS+1; j++) {
            A[i][j] = 0.0;
        }
    }

    // Local boundry condition endpoints
    double tMin = (world_rank)*100.0/world_size;
    double tMax = (world_rank+1)*100.0/world_size;
    double dA_global=100;       // delta t across all processes
    MPI_Status status;          // status returned by MPI calls

    // Left and right boundaries
    for (i = 0; i <= rows_per_proc+1; i++) {
        A[i][0] = 0.0;
        A[i][COLUMNS+1] = tMin + ((tMax-tMin)/ROWS)*i;
    }

    // Top boundary (process 0 only)
    if (world_rank == 0) {
        for (j = 0; j <= COLUMNS+1; j++)
            A[0][j] = 0.0;
    }

    // Bottom boundary
    if (world_rank == world_size-1) {
      for (j=0; j<=COLUMNS+1; j++)
	    A[rows_per_proc+1][j] = (100.0/COLUMNS) * j;
    }

    // Get current wall clock time
    double tstart = MPI_Wtime();

    while (dA_global > TOLERANCE && iterations < MAX_ITERATIONS) {
        iterations++;

        // main calculation: average my four neighbors
	#pragma omp parallel for private(j)
        for (i = 1; i <= rows_per_proc; i++) {
            for(j = 1; j <= COLUMNS; j++) {
                A_new[i][j] = 0.25 * (A[i+1][j] + A[i-1][j] + A[i][j+1] + A[i][j-1]);
            }
        }

        // send bottom real row down
        if (world_rank != world_size-1) { //unless we are bottom process
            MPI_Send(&A_new[rows_per_proc][1], COLUMNS, MPI_DOUBLE, world_rank+1, DOWN, MPI_COMM_WORLD);
        }

        // receive the bottom row from above into our top ghost row
        if (world_rank != 0) { //unless we are top process
            MPI_Recv(&A[0][1], COLUMNS, MPI_DOUBLE, world_rank-1, DOWN, MPI_COMM_WORLD, &status);
        }

        // send top real row up
        if (world_rank != 0) { //unless we are top process
            MPI_Send(&A_new[1][1], COLUMNS, MPI_DOUBLE, world_rank-1, UP, MPI_COMM_WORLD);
        }

        // receive the top row from below into our bottom ghost row
        if (world_rank != world_size-1) { //unless we are bottom process
            MPI_Recv(&A[rows_per_proc+1][1], COLUMNS, MPI_DOUBLE, world_rank+1, UP, MPI_COMM_WORLD, &status);
        }

        double dA = 0.0;
	#pragma omp parallel for private(j) reduction(max: dA)
        for (i = 1; i <= rows_per_proc; i++) {
            for (j = 1; j <= COLUMNS; j++) {
	            dA = fmax(fabs(A_new[i][j]-A[i][j]), dA);
            }
        }

        // exchange matrices
        double** tmp = A;
        A = A_new;
        A_new = tmp;

        // find global dA                                                        
        MPI_Reduce(&dA, &dA_global, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	    MPI_Bcast(&dA_global, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }

    double tend = MPI_Wtime();

    // Slightly more accurate timing and cleaner output 
    MPI_Barrier(MPI_COMM_WORLD);

    // finish timing and output values
    if (world_rank == 0) {
        printf("Max error at iteration %d was %lf\n", iterations-1, dA_global);
        printf("Total time was %lf seconds.\n", tend - tstart);
    }

leave:
    // free A and A_new
    if (A_new != NULL) {
        for (i = 0; i<rows_per_proc+2; i++) {
            if (A_new[i] != NULL) {
                free(A_new[i]);
            }
        }
        free(A_new);
    }

    if (A != NULL) {
        for (i = 0; i<rows_per_proc+2; i++) {
            if (A[i] != NULL) {
                free(A[i]);
            }
        }
        free(A);
    }

    // Finalize the MPI environment.
    MPI_Finalize();

    return error_code;
}
