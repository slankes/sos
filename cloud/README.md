# Simple httpd example

## Create simple web-server on your host

* Install [Docker](https://www.docker.com/get-started/). Beside docker it exists also alternative solution to build OCI containers (e.g. [Podman](https://podman.io))
* Build docker image: `docker build -f Dockerfile -t cloud:latest .`
* Check if image is created and available: `docker images`
* Start web-server with a container: `docker run --rm -p 8080:9975 cloud:latest`. In this case, an access to local port 8080 will be forwarded to port 9975 within the container.
* Take your browser and surf to [http://127.0.0.1:8080](http://127.0.0.1:8080)
* Determine container id `docker ps`
* Stop container: `docker stop CONTAINER_ID`
