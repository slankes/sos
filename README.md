# Special-Purpose Operating Systems

This repository contains example code, which is presentedt with the course _Special-Purpose Operating Systems_ at the RWTH Aachen University, Germany.

Take a look at https://sos.rwth-aachen.de for details.
